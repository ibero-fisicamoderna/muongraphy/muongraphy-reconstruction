from ROOT import TCanvas
from ROOT import TH2Poly
from ROOT import TH1F
from ROOT import kBlue

COLOR_ALPHA = .85


###################################################
# UTILS
###################################################
def make_heatmap_bins(
        th2poly,
        min_x, min_y,
        max_x, max_y,
        n_x, n_y
):
    # MAKE HEAT MAP BINS
    sidex = max_x - min_x
    stepx = sidex / n_x

    sidey = max_y - min_y
    stepy = sidey / n_y

    pos_y = min_y
    pos_y2 = pos_y + stepy

    for i in range(n_y):
        pos_x = min_x
        pos_x2 = pos_x + stepx

        for j in range(n_x):
            th2poly.AddBin(pos_x, pos_y, pos_x2, pos_y2)

            pos_x = pos_x2
            pos_x2 = pos_x + stepx

        pos_y = pos_y2
        pos_y2 = pos_y + stepy


###################################################
# CLASS
###################################################
class Plotter(object):

    def __init__(self, name):
        # INIT
        self.name = name

        self.err2_1d = TH1F('err2_1d_' + self.name, 'Standard Error Dist', 100, 0, 0.005)
        self.dr_dz_1d = TH1F('dr_dz_1d_' + self.name, 'dr/dz', 100, -5, 5)
        self.r_z_eq_0_2d = TH2Poly('r_z_eq_0_2d_' + self.name, 'r at z=0', -15., 15., -15, 15)

        # PLOTS
        make_heatmap_bins(self.r_z_eq_0_2d, -15, -15, 15, 15, 100, 100)

    def add(self, track):
        # FILL PLOTS
        # --------------------------------------------
        self.err2_1d.Fill(track.err2)
        self.dr_dz_1d.Fill(track.calc_dr_dz(0)) # Calcular dr/dz en z=0
        self.r_z_eq_0_2d.Fill(track.xz_fit[1], track.yz_fit[1], 1)

    def write(self):
        # INDIVIDUAL
        # --------------------------------------------
        err2_1d_canvas = TCanvas('err2_1d_' + self.name)
        err2_1d_canvas.cd(0)
        # err2_1d_canvas.SetLogy(1)
        self.err2_1d.SetLineColor(kBlue)
        self.err2_1d.GetXaxis().SetTitle('#err^{2}')
        self.err2_1d.Draw()

        dr_dz_1d_canvas = TCanvas('dr_dz_1d_' + self.name)
        dr_dz_1d_canvas.cd(0)
        self.dr_dz_1d.SetLineColor(kBlue)
        self.dr_dz_1d.GetXaxis().SetTitle('dr/dz [adim]')
        self.dr_dz_1d.Draw()
        
        r_z_eq_0_2d_canvas = TCanvas('r_z_eq_0_2d_' + self.name, '', 1080, 1080)
        r_z_eq_0_2d_canvas.SetRightMargin(0.175)
        r_z_eq_0_2d_canvas.SetLogz(1)
        r_z_eq_0_2d_canvas.cd(0)
        self.r_z_eq_0_2d.GetXaxis().SetTitle('X [cm]')
        self.r_z_eq_0_2d.GetYaxis().SetTitle('Y [cm]')
        self.r_z_eq_0_2d.GetYaxis().SetTitleOffset(1.45)
        self.r_z_eq_0_2d.Draw('COLZ')

        # SAVE
        # --------------------------------------------
        err2_1d_canvas.SaveAs('err2_1d_' + self.name + '.png')
        dr_dz_1d_canvas.SaveAs('dr_dz_1d_' + self.name + '.png')
        r_z_eq_0_2d_canvas.SaveAs('r_z_eq_0_2d_' + self.name + '.png')
