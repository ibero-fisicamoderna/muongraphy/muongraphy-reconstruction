# COMBINACION SOLITA
def generateCombinations(number_of_hits_per_station, n_min_station=2):
    n_stations = len(number_of_hits_per_station)

    roll = False
    combination = [-1] * n_stations
    valid_combinations = []

    # -1: indica que nos podemos brincar el hit
    while not roll:
        # CONTINUITY CHECK
        # Aqui tenemos la combinacion antes de que cambie
        # 1| 2| 3| 4|  <----------------------------- muon
        is_continous = True
        was_present = False

        for station in range(n_stations):
            is_present = combination[station] > -1

            # Si todos los pasados ya estaban presentes, no tiene porque no estarlo ahora
            # De cumplir la condicion, significa que no es continuo
            if was_present and not is_present:
                is_continous = False

            # Recordar si el ultimo estaba presente
            was_present = is_present

        # QUALITY CHECK
        n_present = 0

        for station in range(n_stations):
            if combination[station] > -1:
                n_present += 1

        # USE THE TRACK
        if is_continous and n_present >= n_min_station:
            valid_combinations.append(combination[:])

        # En esta parte se incrementa los indices de la combinacion
        for station in range(n_stations):
            if (combination[station] + 1) < number_of_hits_per_station[station]:
                combination[station] += 1
                break
            else:
                combination[station] = -1

                if station == (n_stations - 1):
                    roll = True
                    break

    # RETURN
    return valid_combinations


# MAIN
def reconstruct(root_file_path):
    from ROOT import TFile
    from muongraphy.trackfinder.tracks import Track, Hit
    from muongraphy.reconstruction.plotter import Plotter

    # BEGIN
    root_file = TFile.Open(root_file_path)

    # LOOP META DATA
    for event in root_file.metadata:
        nEvents = event.nEvents

        print('nEvents: %d' % nEvents)

    # PROCESS EVENTS
    iEvt = 0

    final_tracks = []

    for event in root_file.events:
        nHits = event.nHits

        # SHORT-CIRCUIT
        if nHits == 0:
            continue

        # ORDENAR HITS POR ESTACIONES
        n_stations = 4

        station_hits = {
            0: [],
            1: [],
            2: [],
            3: []
        }

        for iHit in range(nHits):
            hit_station = event.hit_station[iHit]
            hit_time = event.hit_time[iHit]
            hit_posX = event.hit_posX[iHit]
            hit_posY = event.hit_posY[iHit]
            hit_posZ = event.hit_posZ[iHit]

            station_hits[hit_station - 1].append(Hit(
                hit_station,
                hit_time,
                hit_posX,
                hit_posY,
                hit_posZ
            ))

        print(len(station_hits[0]), len(station_hits[1]), len(station_hits[2]), len(station_hits[3]))

        # GENERAR COMBINACIONES
        combinations = generateCombinations(
            [len(station_hits[0]), len(station_hits[1]), len(station_hits[2]), len(station_hits[3])],
            n_min_station=3)

        print('N Combinaciones: %d' % len(combinations))

        # Guardar Combinaciones Validas
        valid_combinations = []

        for combination in combinations:
            # CONVERT COMBINATION INDEX TO HITS
            combination_hits = []

            for station in range(n_stations):
                hit_index = combination[station]

                if hit_index == -1:
                    combination_hits.append(None)
                else:
                    combination_hits.append(station_hits[station][hit_index])

            # VALIDATE COMBINATION
            valid_time = True
            previous_time = None

            for station in range(n_stations):
                current_hit = combination_hits[station]

                if current_hit is not None:
                    current_time = current_hit.time

                    if not (previous_time is None or current_time <= previous_time):
                        valid_time = False
                        break

                    # Guardar el anterior
                    previous_time = current_time

            # APPEND
            if valid_time:
                valid_combinations.append(combination_hits)

        print('Valid Combinations: %d' % len(valid_combinations))

        # Generar Tracks
        event_tracks = []

        for combination in valid_combinations:
            track = Track()

            for hit in combination:
                track.add(hit)

            track.pack()

            if (track.err2 / track.n) > 1e-3:
                continue

            event_tracks.append(track)

        # Guardar Mejores Tracks
        tracks_kept = 0

        n_event_tracks = len(event_tracks)

        this_i_track = 0

        while this_i_track < n_event_tracks:
            this_track = event_tracks[this_i_track]

            keep_this_track = True

            that_i_track = this_i_track + 1

            while that_i_track < n_event_tracks:
                that_track = event_tracks[that_i_track]

                this_overlaps_that = this_track.overlaps(that_track)

                if this_overlaps_that:
                    # REMOVE THIS
                    that_track_longer = this_track.n < that_track.n

                    that_track_has_better_stderr = (that_track.err2 / that_track.n) < \
                                                   (this_track.err2 / this_track.n)

                    if that_track_longer or that_track_has_better_stderr:
                        keep_this_track = False
                        break

                    # REMOVE THAT?
                    this_track_longer = that_track.n < this_track.n

                    this_track_has_better_stderr = (this_track.err2 / this_track.n) < \
                                                   (that_track.err2 / that_track.n)

                    if this_track_longer or this_track_has_better_stderr:
                        event_tracks.pop(that_i_track)
                        n_event_tracks -= 1

                        that_i_track -= 1

                that_i_track += 1

            if keep_this_track:
                tracks_kept += 1
                final_tracks.append(this_track)

            this_i_track += 1

        print('Tracks Kept: %d' % tracks_kept)

        # PRINT SEPARATOR
        print('***')

        iEvt += 1

    # Graficar:
    plotter = Plotter('ex')

    print('Tracks built: %d' % len(final_tracks))

    for track in final_tracks:
        plotter.add(track)
        print('n: %d, xz: %s, yz: %s, err2: %f' % (track.n, track.xz_fit, track.yz_fit, track.err2 / track.n))

    plotter.write()
