import os
import sys

from ROOT import gROOT
from ROOT import kTRUE

if __name__ == '__main__':
    # ADD PROJECT TO PATH
    project_home = os.environ['PROJECT_HOME']
    sys.path.append(project_home + '/src')
    print(project_home)

    # SETUP RUNTIME
    from muongraphy.commons import runtime

    # runtime.debug = args.debug
    runtime.project_home = project_home

    # ROOT
    gROOT.SetBatch(kTRUE)

    # RUN
    from reconstruction import analysis

    analysis.reconstruct('in/test.root')
