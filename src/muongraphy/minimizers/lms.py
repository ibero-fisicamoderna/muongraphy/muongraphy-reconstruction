class LMSMinimizer(object):

    def __init__(self):
        self.n = 0
        self.sum_x = 0
        self.sum_y = 0
        self.sum_xy = 0
        self.sum_x2 = 0

    def add(self, x, y):
        self.n += 1
        self.sum_x += x
        self.sum_y += y
        self.sum_xy += (x * y)
        self.sum_x2 += x ** 2

    def fit(self):
        m = (self.sum_x * self.sum_y - self.n * self.sum_xy) / \
            (self.sum_x ** 2 - self.n * self.sum_x2)

        b = (self.sum_x * self.sum_xy - self.sum_x2 * self.sum_y) / \
            (self.sum_x ** 2 - self.n * self.sum_x2)

        return m, b
