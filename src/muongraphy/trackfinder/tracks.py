import math

from muongraphy.minimizers.lms import LMSMinimizer


class Hit(object):

    def __init__(self, station, time, posX, posY, posZ):
        self.station = station
        self.time = time
        self.posX = posX
        self.posY = posY
        self.posZ = posZ


class Track(object):

    def __init__(self):
        self.n = 0
        self.hits = []
        self.xz_minimizer = LMSMinimizer()
        self.yz_minimizer = LMSMinimizer()

        self.xz_fit = None
        self.yz_fit = None
        self.err2 = None

    def overlaps(self, other):
        for this_hit in self.hits:
            for that_hit in other.hits:
                if this_hit == that_hit:
                    return True

        return False

    def add(self, hit):
        self.hits.append(hit)

        if hit is not None:
            self.n += 1
            self.xz_minimizer.add(hit.posZ, hit.posX)
            self.yz_minimizer.add(hit.posZ, hit.posY)

    def pack(self):
        # Fit
        self.xz_fit = self.xz_minimizer.fit()
        self.yz_fit = self.yz_minimizer.fit()

        # Calculate Standard Error
        err2_xz = 0
        err2_yz = 0

        for hit in self.hits:
            if hit is not None:
                posX = hit.posX
                posY = hit.posY
                posZ = hit.posZ

                err2_xz = (posX - (self.xz_fit[0] * posZ + self.xz_fit[1])) ** 2
                err2_yz = (posY - (self.yz_fit[0] * posZ + self.yz_fit[1])) ** 2

        self.err2 = err2_xz + err2_yz

    def calc_dr_dz(self, z):
        a = self.xz_fit[1]
        c = self.yz_fit[1]

        b = self.xz_fit[0]
        d = self.yz_fit[0]

        a2 = a ** 2
        c2 = c ** 2
        b2 = b ** 2
        d2 = d ** 2
        z2 = z ** 2

        ba = b * a
        dc = d * c

        num = z * (b2 + d2) + (ba + dc)
        dem = math.sqrt(z2 * (b2 + d2) + 2 * z * (ba + dc) + a2 + c2)

        if dem == 0:
            return float("inf")

        return num / dem