def simpleReadout():
    from ROOT import TFile

    # BEGIN
    root_file = TFile.Open('in/test.root')

    # LOOP META DATA
    for event in root_file.metadata:
        nEvents = event.nEvents

        print('nEvents: %d' % nEvents)

    # PROCESS EVENTS
    iEvt = 0

    for event in root_file.events:
        nHits = event.nHits

        # SHORT-CIRCUIT
        if nHits == 0:
            continue

        # GET TRACKS
        for iHit in range(nHits):
            # UNPACK DATA
            # hit_station: Es el numero de RPC que fue golpeada 1| 2| 3| 4|
            # hit_time: Es el tiempo en el que llego el muon a la estacion
            # hit_posX: La posicion global en X
            # hit_posY: La posicion global en Y
            # hit_posZ: La posicion global en Z

            # Metodo de Reconstruccion
            # Empezar solo usando el punto 1 y 2, el 3 es opcional si la tienen grande
            # 1. Muones vienen de afuera hacia adentro
            # 2. El muon no puede brincar estaciones
            # 3. (Opcional aplicar al final - solo si logran reconstruir algo) El muon de la estacion 4 debe llegar antes de la 3, de la 2 y de la 1

            print(iEvt, event.hit_station[iHit], event.hit_time[iHit], event.hit_posX[iHit], event.hit_posY[iHit],
                  event.hit_posZ[iHit])

        iEvt += 1
